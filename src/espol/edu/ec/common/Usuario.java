/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

/**
 *
 * @author andre
 */
public class Usuario {
    protected String nombres;
    protected String apellidos;
    protected String correo;
    protected String organizacion;  
    protected String nombreUsuario;
    protected String clave;
    protected String cedula;

    public Usuario(String nombres, String apellidos, String correo, String organizacion, String nombreUsuario, String clave) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.organizacion = organizacion;
        this.nombreUsuario = nombreUsuario;
        this.clave = clave;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public String getClave() {
        return clave;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public String getNombres() {
        return nombres;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    @Override
    public boolean equals(Object o) {
 
        if(o==null){
            return false;
        }
        if (o.getClass()!=this.getClass()){
            return false;
        }
        if (o==this){
            return true;
        }
        
        Usuario u = (Usuario)o;
        return u.cedula.equals(this.cedula);
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andre
 */
public class Venta {
    private String placaVehiculo;
    private Vehiculo vehiculo;
    private Vendedor vendedor;
    private double precio;
    private ArrayList<Oferta> ofertas;

    public Venta(Vehiculo vehiculo, Vendedor vendedor) {
        this.vehiculo = vehiculo;
        this.vendedor = vendedor;
        this.precio = vehiculo.precio;
        this.placaVehiculo = vehiculo.placa;
        this.ofertas = new ArrayList();
    }

    public ArrayList<Oferta> getOfertas() {
        return ofertas;
    }

    public void setOfertas(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getPlacaVehiculo() {
        return placaVehiculo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPlacaVehiculo(String placaVehiculo) {
        this.placaVehiculo = placaVehiculo;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }
    
    public Oferta revisarOfertas(){
        Scanner sc = new Scanner(System.in);
        int indice = 0;
        int eleccion = 0;
        Oferta o = null;
        do{
            System.out.print(this.ofertas.get(indice));
            System.out.println("1. Siguiente ");
            if(indice > 0) {
                System.out.println("2. Anterior ");
            }
            System.out.println("3. Elegir");
            eleccion = sc.nextInt();
            switch (eleccion) {
                case 1:
                    indice++;
                    break;
                case 2:
                    indice--;
                    break;
                case 3:   
                    System.out.println("Esta oferta ha sido aceptada.");
                    o = this.ofertas.get(indice);
                    break;
                default:
                    break;
            }
        }while(eleccion!= 3);
        return o;
    }
    
    public void aceptarOferta(Oferta o, ArrayList<Venta> ventas, ArrayList<Vehiculo> vehiculos){
        String correo = o.getComprador().getCorreo();
        //se envia el correo de aceptacion
        vehiculos.remove(this.vehiculo);
        ventas.remove(this);
    }
    
    public static Venta comprobarExistencia(ArrayList<Venta> ventas, String placa){
        Venta vent = null;
        for(Venta v : ventas){
            if(v.vehiculo.placa.equals(placa)){
                vent = v;
            }
        }
        return vent;
    }

    @Override
    public String toString() {
        return "Venta{" + "placaVehiculo=" + placaVehiculo + ", vehiculo=" + vehiculo + ", precio=" + precio + '}';
    }
    
    
    

    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andre
 */
public class Vendedor extends Usuario {
    private ArrayList<Venta> ventas;

    public Vendedor(String nombres, String apellidos, String correo, String organizacion, String nombreUsuario, String clave) {
        super(nombres, apellidos, correo, organizacion, nombreUsuario, clave);
    }
    
    public static Vendedor nuevoVendedor(){
        Scanner sc = new Scanner(System.in);
        String nombres;
        String apellidos;
        String correo;
        String organizacion;  
        String nombreUsuario;
        String clave;
        String cedula;
        System.out.println("Ingrese sus nombres");
        nombres = sc.nextLine();
        System.out.println("Ingrese sus apellidos");
        apellidos = sc.nextLine();
        System.out.println("Ingrese su correo electronico");
        correo = sc.nextLine();  
        System.out.println("Ingrese su Organizacion");
        organizacion = sc.nextLine();
        System.out.println("Ingrese su Usuario");
        nombreUsuario = sc.nextLine();
        System.out.println("Ingrese su Clave");
        clave = sc.nextLine();
        Vendedor v = new Vendedor(nombres, apellidos, correo, organizacion, nombreUsuario, clave);
        return v;
    }
    
    public boolean disponibilidad(ArrayList<Vendedor> vendedores){
        boolean disponible = true;
        for(Vendedor v : vendedores){
            if(this.correo.equals(v.correo) || this.nombreUsuario.equals(v.nombreUsuario)){
                disponible = false;
            }
        }
        return disponible;
    }
    
    public static Vendedor comprobarExistencia(ArrayList<Vendedor> vendedores, String usuario, String clave){
        Vendedor vend = null;
        for(Vendedor v : vendedores){
            if(v.nombreUsuario.equals(usuario)&& v.clave.equals(clave)){
                vend = v;
            }
        }
        return vend;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;
import java.util.ArrayList;
import java.util.Scanner;
/**
 *
 * @author andre
 */
public class Comprador extends Usuario{
    private ArrayList<Venta> ventas;

    public Comprador(String nombres, String apellidos, String correo, String organizacion, String nombreUsuario, String clave) {
        super(nombres, apellidos, correo, organizacion, nombreUsuario, clave);
    }

    @Override
    public String toString() {
        return super.toString() + "lol"; //To change body of generated methods, choose Tools | Templates.
    }
    
    public static Comprador nuevoComprador(){
        Scanner sc = new Scanner(System.in);
        String nombres;
        String apellidos;
        String correo;
        String organizacion;  
        String nombreUsuario;
        String clave;
        String cedula;
        System.out.println("Ingrese sus nombres");
        nombres = sc.nextLine();
        System.out.println("Ingrese sus apellidos");
        apellidos = sc.nextLine();
        System.out.println("Ingrese su correo electronico");
        correo = sc.nextLine();  
        System.out.println("Ingrese su Organizacion");
        organizacion = sc.nextLine();
        System.out.println("Ingrese su Usuario");
        nombreUsuario = sc.nextLine();
        System.out.println("Ingrese su Clave");
        clave = sc.nextLine();
        return new Comprador(nombres, apellidos, correo, organizacion, nombreUsuario, clave);
    }
    public boolean disponibilidad(ArrayList<Comprador> compradores){
        boolean disponible = true;
        for(Comprador c : compradores){
            if(this.correo.equals(c.correo) || this.nombreUsuario.equals(c.nombreUsuario)){
                disponible = false;
            }
        }
        return disponible;
    }
    public static Comprador comprobarExistencia(ArrayList<Comprador> compradores, String usuario, String clave){
        Comprador comp = null;
        for(Comprador c : compradores){
            if(c.nombreUsuario.equals(usuario)&& c.clave.equals(clave)){
                comp = c;
            }
        }
        return comp;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.common;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andre
 */
public class Vehiculo {
    protected String placa;
    protected String modelo;
    protected String tipoMotor;
    protected double recorrido;
    protected String color;
    protected double precio;
    protected String tipo;
    protected int año;

    public Vehiculo(String placa, String modelo, String tipoMotor, double recorrido, String color, double precio, String tipo, int año) {
        this.placa = placa;
        this.modelo = modelo;
        this.tipoMotor = tipoMotor;
        this.recorrido = recorrido;
        this.color = color;
        this.precio = precio;
        this.tipo = tipo;
        this.año = año;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "placa=" + placa + "\n modelo=" + modelo + "\n tipoMotor=" + tipoMotor + "\n recorrido=" + recorrido + "\n color=" + color + "\n precio=" + precio + "\n tipo=" + tipo + "\n año" + año + '}';
    }

    
    
    

    @Override
    public boolean equals(Object o) {
        if(o==null){
            return false;
        }
        if (o.getClass()!=this.getClass()){
            return false;
        }
        if (o==this){
            return true;
        }
        
        Vehiculo v = (Vehiculo)o;
        return v.placa.equals(this.placa);
    }
    
    
    public boolean disponibilidad(ArrayList<Vehiculo> vehiculos){
        boolean disponible = true;
        for(Vehiculo vehiculo: vehiculos){
            if(this.equals(vehiculo)){
                disponible = false;
            }
        }
        return disponible;
    }

    public String getModelo() {
        return modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public double getPrecio() {
        return precio;
    }

    public double getRecorrido() {
        return recorrido;
    }
    

}

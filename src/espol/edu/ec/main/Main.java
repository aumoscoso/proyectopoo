/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.main;

import espol.edu.ec.common.Camion;
import espol.edu.ec.common.Camioneta;
import espol.edu.ec.common.Carro;
import espol.edu.ec.common.Comprador;
import espol.edu.ec.common.Motocicleta;
import espol.edu.ec.common.Oferta;
import espol.edu.ec.common.Vehiculo;
import espol.edu.ec.common.Vendedor;
import espol.edu.ec.common.Venta;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * p 0 ? [] { =
 * @author andre
 */
public class Main {
    private static Scanner sc = new Scanner(System.in);
    private static int eleccion = 0;
    private static ArrayList<Vendedor> vendedores = new ArrayList();
    private static ArrayList<Comprador> compradores = new ArrayList();
    private static ArrayList<Vehiculo> vehiculos = new ArrayList();
    private static ArrayList<Venta> ventas = new ArrayList();
    public static void main(String args[]){
        do{
            System.out.println("1. Vendedor");
            System.out.println("2. Comprador");
            System.out.println("3. Salir");
            eleccion = sc.nextInt();
            if(eleccion==1){   
                do{
                    System.out.println("1. Registrar un nuevo vendedor");
                    System.out.println("2. Ingresar un nuevo vehículo");
                    System.out.println("3. Aceptar oferta");
                    System.out.println("4. Regresar");
                    eleccion = sc.nextInt();
                    switch (eleccion) {
                        case 1:
                            {
                                Vendedor v = Vendedor.nuevoVendedor();
                                if(v.disponibilidad(vendedores)){
                                    vendedores.add(v);
                                    System.out.println("Registrado exitosamente.");
                                }else{
                                    System.out.println("\n El correo y usuario ya se encuentran registrados, intente nuevamente \n");
                                }       break;
                            }
                        case 2:
                            String usuario;
                            String clave;
                            System.out.println("Ingrese su usuario");
                            usuario = sc.nextLine();
                            usuario = sc.nextLine();
                            System.out.println("Ingrese su clave");
                            clave = sc.nextLine();
                            Vendedor vend = Vendedor.comprobarExistencia(vendedores, usuario, clave);
                            if(vend!=null){
                                System.out.println("Ingrese el tipo de vehiculo");
                                String tipo = sc.nextLine();
                                tipo = tipo.substring(0,1).toUpperCase() + tipo.substring(1);
                                switch (tipo) {
                                    case "Carro":
                                    {
                                        Carro c = Carro.nuevoCarro();
                                        Venta v = new Venta(c,vend);
                                        if(c.disponibilidad(vehiculos)){
                                            vehiculos.add(c);
                                            ventas.add(v);
                                            
                                            System.out.println("Registrado exitosamente");
                                            
                                        } else {
                                            System.out.println("Este vehiculo ya existe, intente de nuevo");
                                        }       break;
                                    }
                                    case "Camion":
                                    {
                                        Camion c = Camion.nuevoCamion();
                                        Venta v = new Venta(c,vend);
                                        if(c.disponibilidad(vehiculos)){
                                            vehiculos.add(c);
                                            ventas.add(v);
                                            System.out.println("Registrado exitosamente");
                                            
                                        } else {
                                            System.out.println("Este vehiculo ya existe, intente de nuevo");
                                        }       break;
                                    }
                                    case "Camioneta":
                                    {
                                        Camioneta c = Camioneta.nuevaCamioneta();
                                        Venta v = new Venta(c,vend);
                                        if(c.disponibilidad(vehiculos)){
                                            vehiculos.add(c);
                                            ventas.add(v);
                                            System.out.println("Registrado exitosamente");
                                            
                                        } else {
                                            System.out.println("Este vehiculo ya existe, intente de nuevo");
                                        }       break;
                                    }
                                    case "Motocicleta":
                                    {
                                        Motocicleta m = Motocicleta.nuevaMoto();
                                        Venta v = new Venta(m,vend);
                                        if(m.disponibilidad(vehiculos)){
                                            vehiculos.add(m);
                                            ventas.add(v);
                                            System.out.println("Registrado exitosamente");
                                        } else {
                                            System.out.println("Este vehiculo ya existe, intente de nuevo");
                                        }       break;
                                    }
                                    default:
                                        System.out.println("No se registra este tipo de vehiculo, intente de nuevo");
                                        break;
                                }
                            }else{
                                System.out.println("No se encontro esta combinacion, intente de nuevo o registrese");
                            }   break;
                        case 3:
                            {
                                
                                System.out.println("Ingrese la placa del vehiculo");
                                String placas = sc.nextLine();
                                placas = sc.nextLine();
                                Venta v = Venta.comprobarExistencia(ventas, placas);
                                if(v == null){
                                    System.out.println("No se encuentra un vehiculo con esa placa");
                                    break;
                                }
                                System.out.println(v.getVehiculo().getModelo() + "Precio: " + v.getVehiculo().getPrecio());
                                Oferta o = v.revisarOfertas();
                                v.aceptarOferta(o, ventas, vehiculos);
                                break;
                            }
                        default:
                            break;
                    }
                }while(eleccion != 4);
            }
            
            if(eleccion==2){
                do{
                    System.out.println("1. Registrar un nuevo comprador");
                    System.out.println("2. Ofertar por un vehiculo");
                    System.out.println("3. Regresar");
                    eleccion = sc.nextInt();
                    if(eleccion==1){
                        Comprador c = Comprador.nuevoComprador();
                        if(c.disponibilidad(compradores)){
                           compradores.add(c);
                           System.out.println("Registrado exitosamente.");
                        }else{
                            System.out.println("\n El correo y usuario ya se encuentran registrados, intente nuevamente \n");
                        }
                    }if(eleccion==2){
                        String usuario;
                        String clave;
                        System.out.println("Ingrese su usuario");
                        usuario = sc.nextLine();
                        usuario = sc.nextLine();
                        System.out.println("Ingrese su clave");
                        clave = sc.nextLine();
                        Comprador c = Comprador.comprobarExistencia(compradores, usuario, clave);
                        if(c != null){
                            Venta v = Oferta.elegirVenta(ventas);
                            Oferta o = new Oferta(c,v);
                            o.realizarOferta();
                            
                        }else{
                            System.out.println("No se encontro esta combinacion, intente de nuevo o registrese");
                        }
                    }
                }while(eleccion!=3 );
                eleccion = 0;
            }
        }while(eleccion != 3);
    }
}
